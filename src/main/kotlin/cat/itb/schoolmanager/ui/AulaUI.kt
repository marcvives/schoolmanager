package cat.itb.schoolmanager.ui

import cat.itb.schoolmanager.model.Alumne
import cat.itb.schoolmanager.model.UF
import java.util.*

/**
 * Classe que ens permetra gestionar una aula, utilitza un Scanner
 * per llegir les dades de l'usuari (professor d'aula)
 *
 * @param sc Scanner d'on es llegiran les dades del teclat
 */
class AulaUI (sc: Scanner, alumnes: List<Alumne>, uf: UF){

    /**
     * Anota una [hores] de falta a l'[alumne]
     * @param alumne [Alumne] al qual es posa falta o no.
     * @param hores Numero d'hores que ha faltat en aquesta assignatura
     * @return Boleà, true si ha superat el màxim de faltes.
     */
    fun marcarFalta(alumne: Alumne, hores: Int):Boolean{
        return false
    }

    /**
     * Passa llista de forma aleatoria, permet marcar falta
     *
     * Mostra el nom de cada l'alumne i llegeix un caràcter del Scanner.
     * Si llegeix una "F" o "f", li posarà una falta.
     */
    fun passarLlistaRandom(){
        //TODO: Recorrer la llita de forma aleatoria
        //TODO: mostrar nom de l'alumne
        //TODO: Si es llegeix "X" es marca falta
    }

    /**
     * Mostra el nom complet de l'alumne
     * @param alumne Alumne de l'aula
     */
    fun mostrarNomAlumne(alumne: Alumne){

    }
}