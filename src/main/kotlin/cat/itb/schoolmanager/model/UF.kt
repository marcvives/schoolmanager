package cat.itb.schoolmanager.model

import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.*


/**
 * Data class per mantenir la informació d'una Unitat Formativa
 *
 * @param name Nom de la unitat formativa
 * @param code Codi curt de la Unitat formativa, exemple: MP03
 * @param horesTotals Total d'hores que dura la Unitat Formativa
 * @param dataInici LocalDate amb la data d'inici de la Unitat Formativa
 * @param dataFi LocalDate amb la data fi de la Unitat Formativa
 *
 * @see Date
 * @author Programer Name
 */
data class UF(val name: String, val code: String, val horesTotals: Int, val dataInici: LocalDate, val dataFi:LocalDate){

    /**
     * Returns the duration of the UF in days
     */
    val days get() = ChronoUnit.DAYS.between(dataInici, dataFi);
}
