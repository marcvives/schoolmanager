package cat.itb.schoolmanager.model

/**
 * Gestionar la informació de l'alumne
 *
 * @param id identificador únic de l'alumne
 * @param name Nom del alumne
 * @param cognoms List amb els cognoms.
 *
 * @author Programer Name
 */
data class Alumne(val id: String, var name: String, var cognoms: List<String>) {
    /**
     * Llista amb les faltes de l'alumne
     * @see Faltes
     */
    val faltes: MutableList<Faltes> = mutableListOf()
    
    /**
     * Afegeix hores de falta a la uf informada
     *
     * @param ufCode codi de la Unitat Formativa
     * @param hores hores que s'han d'anotar
     * @return total d'hores acumulades
     */
    fun anotarFalta(ufCode: String, hores: Int):Int{
        //TODO: obtenir la UF, afegir falta i retornar total
        return 0
    }

}