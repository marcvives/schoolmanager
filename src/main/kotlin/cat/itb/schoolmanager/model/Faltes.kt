package cat.itb.schoolmanager.model

/**
 * Data class per desar la informació de les faltes/absències
 *
 * @param ufCode Codi de la Unitat Formativa on s'ha generat la falta
 * @param amount Total de faltes generades en la Unitat Formativa
 * @see UF
 * @author Programer Name
 */
data class Faltes(val ufCode: String, var amount: Int){
}