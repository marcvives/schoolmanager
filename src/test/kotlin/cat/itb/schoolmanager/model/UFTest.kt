package cat.itb.schoolmanager.model

import org.junit.jupiter.api.Test
import java.time.LocalDate
import kotlin.test.assertEquals


internal class UFTest {

    @Test
    fun getDays() {
        val uf = UF("name", "code", 245, LocalDate.of(2022, 10, 12), LocalDate.of(2022, 12, 2))
        assertEquals(51, uf.days)
    }
}